/* acolors.h -- LogWatcher

   This file is part of the LogWatcher tool.

   Copyright (C) 2002-2009 Artur Robert Czechowski

   The LogWatcher is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of
   the License, or (at your option) any later version.

   The LogWatcher is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with the LogWatcher; see the file COPYING.
   If not, write to the Free Software Foundation, Inc.,
   59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

   Artur R. Czechowski
   <arturcz@hell.pl>
   http://hell.pl/arturcz/
 */

#ifndef _ARC_ACOLORS_H
#define _ARC_ACOLORS_H

/* ANSI colors */
#define CLEAR		"[0m"
#define RESET		"[0m"
#define BOLD		"[1m"
#define REVERSE		"[7m"
#define CONCEALED	"[8m"

#define BLACK		"[0;30m"
#define RED		"[0;31m"
#define GREEN		"[0;32m"
#define BROWN		"[0;33m"
#define BLUE		"[0;34m"
#define MAGENTA		"[0;35m"
#define	CYAN		"[0;36m"
#define LIGHTGRAY	"[0;37m"

#define DARKGRAY	"[1;30m"
#define	BRIGHTRED	"[1;31m"
#define BRIGHTGREEN	"[1;32m"
#define YELLOW		"[1;33m"
#define BRIGHTBLUE	"[1;34m"
#define PURPLE		"[1;35m"
#define BRIGHTCYAN	"[1;36m"
#define WHITE		"[1;37m"

#define _N_NATURAL 0
#define _N_CLEAR 1
#define _N_RESET _N_CLEAR
#define _N_BOLD 2
#define _N_REVERSE 3
#define _N_CONCEALED 4

#define _N_BLACK 5
#define _N_RED 6
#define _N_GREEN 7
#define _N_BROWN 8
#define _N_BLUE 9
#define _N_MAGENTA 10
#define _N_CYAN 11
#define _N_LIGHTGRAY 12

#define _N_DARKGRAY 13
#define	_N_BRIGHTRED 14
#define _N_BRIGHTGREEN 15
#define _N_YELLOW 16
#define _N_BRIGHTBLUE 17
#define _N_PURPLE 18
#define _N_BRIGHTCYAN 19
#define _N_WHITE 20

#include "data.h"

extern const char *colmode[];
extern const struct s_str_to_val colstr[];

int ascprintf(char **str, char *format);
int cprintf(char *format);

#endif
