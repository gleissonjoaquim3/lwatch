/* strpcre.c -- LogWatcher

   This file is part of the LogWatcher tool.

   Copyright (C) 2002-2009 Artur Robert Czechowski

   The LogWatcher is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of
   the License, or (at your option) any later version.

   The LogWatcher is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with the LogWatcher; see the file COPYING.
   If not, write to the Free Software Foundation, Inc.,
   59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

   Artur R. Czechowski
   <arturcz@hell.pl>
   http://hell.pl/arturcz/
 */

#include "config.h"

#include <pcre.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "control.h"
#include "acolors.h"
#include "settings.h"
#include "strpcre.h"

/* We change an idea
#define DATE_COLOR colmode[date_c]
#define HOST_COLOR colmode[host_c]
#define SERV_COLOR colmode[serv_c]
#define MESG_COLOR colmode[mesg_c]
#define HL_COLOR colmode[hl_c]
*/

#define DATE_COLOR date_c
#define HOST_COLOR host_c
#define SERV_COLOR serv_c
#define MESG_COLOR mesg_c
#define HL_COLOR hl_c

struct {
    pcre *pre;
    pcre_extra *prh;
} parser;

struct {
    int start, stop;
} re_matches[RE_NMATCHES];

int date_c, host_c, serv_c, mesg_c, hl_c, hl_num; /* hl_num stores number of highlights in current line */

int compile_re(char *re, pcre **pre, pcre_extra **prh) {
    const char *re_error;
    int re_errno;

#ifdef DEBUG
    fprintf(stderr, "Compiling pattern: %s\n", re);
#endif
    *pre = pcre_compile(re, PCRE_COPT, &re_error, &re_errno, NULL);
    if (*pre == NULL) {
        fprintf(stderr, "Regexp: %s\nCompile error %s at offset %d\n",
                re, re_error, re_errno);
        return(0);
    }
    *prh = pcre_study(*pre, 0, &re_error);
    if (re_error != NULL) {
        fprintf(stderr, "Regexp: %s\nStuding error %s\n", re, re_error);
        return(0);
    }
    return(1);
}

void free_re(pcre **pre, pcre_extra **prh) {
    pcre_free(*pre);
    pcre_free(*prh);
}

void init_parser(void) {
    if (!compile_re(PARSELINE, &parser.pre, &parser.prh)) {
        die("Cannot compile parser regexp. Exiting\n");
    }
#ifdef DEBUG
    fprintf(stderr, "Parser initialized\n");
#endif
}

void free_parser(void) {
    free_re(&parser.pre, &parser.prh);
#ifdef DEBUG
    fprintf(stderr, "Parser freed\n");
#endif
}

char *newstr(const char *str) {
    char *ptmp;
    ptmp = (char *)malloc(strlen(str) + 1);
    if (ptmp == NULL) {
        die("Cannot allocate %i byte(s) in newstr()\n", strlen(str) + 1);
    }
    strcpy(ptmp, str);
    return(ptmp);
}

char *addstr(char *s1, const char *s2) {
    char *ptmp;
    ptmp = (char *)realloc((void *)s1, strlen(s1) + strlen(s2) + 1);
    if (ptmp == NULL) {
        die("Cannot reallocate %i byte(s) in addstr()\n",
            strlen(s1) + strlen(s2) + 1);
    }
    strcat(ptmp, s2);
    return(ptmp);
}

char *hl_str(struct s_action *act, char *str) {
    static char zero[] = "^00";
    char *pmatch, *pres;
    char ctrl[4];
    int i, j;
    int start_match;
    pmatch = newstr(str);
    start_match = 0;
    pres = pmatch;
    sprintf(ctrl, "^%02i", HL_COLOR);
    while (pcre_exec(act->re, act->rh, pmatch, strlen(pmatch), start_match, 0, (int *)&re_matches, RE_NMATCHES) > 0) {
        hl_num++;
        pres = (char *)malloc(strlen(pmatch) + 7); /* ^cc ^00 NULL */
        i = j = 0;
        while (j < re_matches->start) {
            pres[i++] = pmatch[j++];
        }
        j = 0;
        while ((pres[i++] = ctrl[j++])) {
            ;
        }
        i--;
        j = re_matches->start;
        while (j < re_matches->stop) {
            pres[i++] = pmatch[j++];
        }
        pres[i] = '\0'; /* is this necessary? */
        j = 0;
        while ((pres[i++] = zero[j++])) {
            ;
        }
        i--;
        j = re_matches->stop;
        while ((pres[i++] = pmatch[j++])) {
            ;
        }
        free(pmatch);
        pmatch = pres;
        start_match = re_matches->stop + 4;
    }
    return(pres);
}

void showline(char *input) {
    int i, n, ignore;
    const char **re_str;
    char *match;
    char *date_s, *host_s, *serv_s, *mesg_s;

    //memcpy((void*)&date_c,(void*)&lw_conf.def_date_color,4*sizeof(int));
    date_c = lw_conf.def_date_color;
    host_c = lw_conf.def_host_color;
    serv_c = lw_conf.def_serv_color;
    mesg_c = lw_conf.def_mesg_color;
    hl_c = 0;
    ignore = 0;
    hl_num = 0;
    n = pcre_exec(parser.pre, parser.prh, input, strlen(input), 0, 0,
                  (int *)&re_matches, RE_NMATCHES);
    if (n < 0) {
        if (lw_conf.show_unparsed) {
            printf("%s\n", input);
        }
        return;
    }
    pcre_get_substring_list(input, (int *)&re_matches, n, &re_str);
    date_s = newstr(re_str[1]);
    host_s = newstr(re_str[2]);
    serv_s = newstr(re_str[3]);
    mesg_s = newstr(re_str[4]);
    pcre_free_substring_list(re_str);
    for (i = 0; i < no_actions; i++) {
        if (lw_actions[i].match_service) {
            match = serv_s;	/* match service */
        } else if (lw_actions[i].match_host) {
            match = host_s;	/* match hostname body */
        } else {
            match = mesg_s;	/* match message body */
        }
        n = pcre_exec(lw_actions[i].re, lw_actions[i].rh,
                      match, strlen(match), 0, 0,
                      (int *)&re_matches, RE_NMATCHES);
        if (n > 0) {
            ignore = lw_actions[i].ignore;
            if (lw_actions[i].date_color) {
                date_c = lw_actions[i].date_color;
            }
            if (lw_actions[i].host_color) {
                host_c = lw_actions[i].host_color;
            }
            if (lw_actions[i].serv_color) {
                serv_c = lw_actions[i].serv_color;
            }
            if (lw_actions[i].mesg_color) {
                mesg_c = lw_actions[i].mesg_color;
            }
            if (lw_actions[i].highlight_color) {
                hl_c = lw_actions[i].highlight_color;
                match = hl_str(&lw_actions[i], mesg_s);
                free(mesg_s);
                mesg_s = match;
            }
            if (action_exit(lw_actions[i])) { /* dont check other actions */
                break;
            }
        }
    }
#ifdef DEBUG
    /* printf("all: %i, cur: [%i] %s\n",no_actions,i,lw_actions[i].restr); */
#endif
    if (!ignore) {
        char *cstr;
        int len = strlen(date_s) + strlen(host_s) + strlen(serv_s) + strlen(mesg_s) + 17;
        cstr = (char *)malloc(len);
        sprintf(cstr, "^%02i%s^%02i%s^%02i%s^%02i%s^01\n",
                DATE_COLOR, date_s,
                HOST_COLOR, host_s,
                SERV_COLOR, serv_s,
                MESG_COLOR, mesg_s);
        cprintf(cstr);
        free((void *)cstr);
    }
    free(date_s);
    free(host_s);
    free(serv_s);
    free(mesg_s);
}
