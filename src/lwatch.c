/* lwatch.c -- LogWatcher

   This file is part of the LogWatcher tool.

   Copyright (C) 2002-2009 Artur Robert Czechowski

   The LogWatcher is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of
   the License, or (at your option) any later version.

   The LogWatcher is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with the LogWatcher; see the file COPYING.
   If not, write to the Free Software Foundation, Inc.,
   59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

   Artur R. Czechowski
   <arturcz@hell.pl>
   http://hell.pl/arturcz/
 */

#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <sys/poll.h>
#include <sys/stat.h>
#include <sys/types.h>

#include "config.h"
#include "control.h"
#include "daemon.h"
#include "defaults.h"
#include "log.h"
#include "settings.h"
#include "strpcre.h"

#define BUFSIZE 1024

int main(int argc, char **argv) {
    int fd;
    int res;
    char rbuf[BUFSIZE + 1];
    char *pbuf, *pstart, *pstop;
    struct pollfd input;
    int checkeof;

    checkeof = 0;
    parse_options(argc, argv);
    parse_config();
    /*** Tymczasowo ***/
    if (!lw_conf.in_file) {
        lw_conf.in_file = strdup(DEF_IN_FILE);
    }
    if (!lw_conf.out_file) {
        lw_conf.out_file = strdup(DEF_OUT_FILE);
    }
    /******************/
    set_handlers();
    init_parser();
    if (strcmp(lw_conf.in_file, "-") == 0) {
        fd = 0;
#ifdef DEBUG
        fprintf(stderr, "Input from stdin\n");
#endif
    } else {
        struct stat sbuf;
        io_check(stat(lw_conf.in_file, &sbuf), lw_conf.in_file);
        if (!S_ISFIFO(sbuf.st_mode)) {
            die("ERROR: Regular file are not supported for now. See manual for details\n");
        }
        io_check(fd = open(lw_conf.in_file, O_RDONLY), lw_conf.in_file);
#ifdef DEBUG
        fprintf(stderr, "Input from %s\n", lw_conf.in_file);
#endif
    }
    if (strcmp(lw_conf.out_file, "-") == 0) {
#ifdef DEBUG
        fprintf(stderr, "Output to stdout\n");
#endif
    } else {
        io_fcheck(freopen(lw_conf.out_file, "w", stdout), lw_conf.out_file);
        /* io_check(fd=open(lw_conf.out_file,O_WRONLY),lw_conf.out_file); */
#ifdef DEBUG
        fprintf(stderr, "Output to %s\n", lw_conf.out_file);
#endif
    }
    loop = 1;
    pbuf = newstr("");
    input.fd = fd;
    input.events = POLLIN | POLLHUP;

    start_log();

    if (lw_conf.daemon_mode) {
        daemonize();
    } else {
        send_log(LOG_DEBUG, "Program started, pid: %d", getpid());
    }

    while (loop) {
        input.revents = 0;
        res = poll(&input, 1, -1);
        if (errno != EINTR) {
            io_check(res, "poll");
        }
        if (input.revents == POLLHUP) {
            checkeof = 1;
        }
        if (checkeof) {
            checkeof = 0;
            /* other end closed fifo */
            if (fd) {
                /* from fifo */
#ifdef DEBUG
                fprintf(stderr, "syslog closed second end of fifo %s\nTrying to reopen\n", lw_conf.in_file);
#endif
                /* trying to reopen or die */
                io_check(fd = open(lw_conf.in_file, O_RDONLY), lw_conf.in_file);
                input.fd = fd;
            } else {
                /* from stdin */
#ifdef DEBUG
                fprintf(stderr, "end of file on stdin, exitting...\n");
#endif
                loop = 0;
            }
            continue;
        }
        if (res < 1) {
            continue;
        }
        io_check(res = read(fd, (void *)rbuf, BUFSIZE), "read input");
        if (!res) {
            checkeof = 1;
            continue;
        }
        rbuf[res] = '\0';
        pstart = pbuf = addstr(pbuf, rbuf);
        while ((pstop = strchr(pstart, '\n')) != NULL) {
            (*pstop++) = '\0';
            showline(pstart);
            pstart = pstop;
        }
        pstart = newstr(pstart);
        freestr(pbuf);
        pbuf = pstart;
    }
    send_log(LOG_DEBUG, "Exit program, clean up resources");
    if (fd) {
        close(fd);
    }
    free_parser();
    free_settings();
    exit(0);
}
