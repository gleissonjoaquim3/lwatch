AC_PREREQ(2.57)
AC_INIT([lwatch], [0.6.2])
AC_CONFIG_SRCDIR(src/lwatch.c)

AC_CANONICAL_BUILD
AC_CANONICAL_HOST
AC_CANONICAL_TARGET

AM_INIT_AUTOMAKE([dist-bzip2 no-dist-gzip])
PKG_PROG_PKG_CONFIG
if test -z "${PKG_CONFIG}"; then
    AC_MSG_ERROR([cannot find pkg-config])
fi

CFLAGS="-Wall ${CFLAGS}"

AC_PREFIX_DEFAULT(/usr/local)
test x"$prefix" = xNONE && prefix="$ac_default_prefix"

AM_CONFIG_HEADER(src/config.h)

AH_TEMPLATE([ALLPARAMS],
	[./configure options])

AH_TEMPLATE([PIDFILE],
	[./configure options])

AH_TEMPLATE([DEBUG],
	[Define to 1 if you want to compile with debug support.])

AH_TEMPLATE([SYSCONFDIR],
	[Directory where configuration file is placed])

AH_TEMPLATE([INPUTFILE],
	[Default input file])

AC_MSG_CHECKING(for platform dependent directories)
case $target in
    *-*-*bsd*)
        CPPFLAGS="${CPPFLAGS} -I/usr/local/include"
	    LDFLAGS="${LDFLAGS} -L/usr/local/lib"
	    AC_MSG_RESULT(add /usr/local)
	;;
    *)
    	AC_MSG_RESULT(none)
	;;
esac

dnl Checks for programs.
AC_PROG_CC
AC_PROG_INSTALL
AC_PROG_LN_S
AM_PROG_LEX
AC_PATH_PROG(SED,[sed],[no])
if test "$SED" = "no"; then
    AC_MSG_ERROR([sed not found])
fi

dnl AC_PATH_PROG(dbook2man,[docbook-to-man],[no])
dnl if test "$dbook2man" = "no"; then
dnl     unset ac_cv_path_dbook2man
dnl     AC_PATH_PROG(dbook2man,[docbook2man],[no])
dnl     if test "$dbook2man" = "no"; then
dnl         AC_MSG_WARN([docbook-to-man not found, using lwatch.1.in stub])
dnl     else
dnl         dbook2man_out=""
dnl         dbook2man_out_in="&& mv -f lwatch.1 lwatch.1.in"
dnl 	AC_MSG_WARN([you could have bad formatted manual page, see README for details])
dnl 	sleep 5
dnl     fi
dnl else
dnl   dbook2man_out="> lwatch.1"
dnl   dbook2man_out_in="> lwatch.1.in"
dnl fi
dnl AC_SUBST(dbook2man_out)
dnl AC_SUBST(dbook2man_out_in)
dnl if test "$dbook2man" != "no"; then
dnl   AC_MSG_NOTICE([sgml->man: ${dbook2man} lwatch.sgml ${dbook2man_out}])
dnl fi

AC_PATH_PROG(dbook2man,[docbook-to-man],[no])
if test "$dbook2man" = "no"; then
    AC_MSG_WARN([docbook-to-man not found, using lwatch.1.in stub if possible])
else
    dbook2man_out=">"
fi
AC_SUBST(dbook2man_out)
if test "$dbook2man" != "no"; then
    AC_MSG_NOTICE([sgml->man: ${dbook2man} file.sgml ${dbook2man_out} file.man])
fi

PKG_CHECK_MODULES([pcre], [libpcre], [], [AC_MSG_ERROR([Error: libpcre not installed. Install first.])])
LIBS="$LIBS $pcre_LIBS"
CPPFLAGS="$CPPFLAGS $pcre_CFLAGS"

dnl Checks for header files.
AC_HEADER_STDC
AC_CHECK_HEADERS([errno.h fcntl.h getopt.h pcre.h signal.h stdarg.h stddef.h stdio.h stdlib.h string.h sys/param.h sys/poll.h sys/stat.h sys/types.h unistd.h])

dnl Checks for typedefs, structures, and compiler characteristics.
dnl AC_C_CONST

dnl Checks for library functions.
AC_FUNC_MALLOC
AC_FUNC_REALLOC
AC_TYPE_SIGNAL
AC_FUNC_VPRINTF
AC_CHECK_FUNCS([memset strcasecmp strchr])

AC_CHECK_FUNC([getopt_long])
if test "$ac_cv_func_getopt_long" = "no"; then
  AC_CHECK_LIB([gnugetopt],[getopt_long],LIBS="${LIBS} -lgnugetopt",AC_MSG_ERROR([Error: getopt_long unavailable]))
fi

dnl Define autoheader templates
dnl Checks for user options
AC_MSG_CHECKING(for compile with diagnostic messages)
AC_ARG_ENABLE(debug,
	AC_HELP_STRING([--enable-debug],[Compile with diagnostic messages]), ,
	[enable_debug="no"])
AC_MSG_RESULT($enable_debug)
if test "$enable_debug" = "yes"; then
    AC_DEFINE(DEBUG)
fi

arc_sysconfdir=`eval echo $sysconfdir`
AC_DEFINE_UNQUOTED(SYSCONFDIR,"$arc_sysconfdir")

AM_CONDITIONAL(HAVE_D2M, test "$dbook2man" != "no")

arc_localstatedir=`eval echo $localstatedir`
AC_MSG_CHECKING(for default input file)
AC_ARG_ENABLE(input,
	AC_HELP_STRING([--enable-input=file],[Specify default input file other than ${localstatedir}/lib/syslog.fifo]), ,[enable_input="yes"])
if test "$enable_input" = "no"; then
  enable_input=${arc_localstatedir}/lib/syslog.fifo
fi
if test "$enable_input" = "yes"; then
  enable_input=${arc_localstatedir}/lib/syslog.fifo
fi
AC_MSG_RESULT($enable_input);
INPUTFILE=$enable_input
AC_SUBST(INPUTFILE)
AC_DEFINE_UNQUOTED(INPUTFILE,"${INPUTFILE}")

dnl AC_DEFINE([ALLPARAMS],[],[Parameters from ./configure])
ALLPARAMS="${ac_configure_args}"
AC_SUBST(ALLPARAMS)
AC_DEFINE_UNQUOTED(ALLPARAMS,"${ALLPARAMS}")

# temporary FIXME
PIDFILE=/var/run/lwatch.pid
AC_SUBST(PIDFILE)
AC_DEFINE_UNQUOTED(PIDFILE,"${PIDFILE}")

dnl Epilog
AC_CONFIG_FILES([src/Makefile doc/Makefile Makefile])
AC_OUTPUT
